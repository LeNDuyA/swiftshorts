
extension Substring {
    var assc: Character {
        if self == "^" { return "R" }
        return "L"
    }
    var isLBracket: Bool {
        return self == "("
    }
    var isRBracket: Bool {
        return self == ")"
    }
    var isOperator: Bool {
        return ("+ - / * ^" as String).contains(self)
    }
    var isNumber: Bool {
        if let _ = Double(self) { return true }
        return false
    }
    var prec: Int {
        switch self {
        case "^":
            return 4
        case "*", "/":
            return 3
        case "+", "-":
            return 2
        default:
            return 0
        }
    }
}

class Postfix {

    var expr: String?

    init(expr input: String?) {
        if input == nil {
            expr = loadExpr()
        } else {
            expr = input!
        }

        expr = RPN(origExpr: expr!)
    }


    private func RPN(origExpr input: String) -> String {
        //

        var operators = Stack<Substring>()
        var output = Queue<Substring>()

        //debugging
        var outputStr = ""
        var operatorStr = ""

        for token in input.split(separator: " "){
            if token.isNumber {
                output.enqueue(token)
//                outputStr = ToString(output.EmptyOut())
            } else if token.isOperator {
                operatorOp(&output, &operators, token)
//                outputStr = ToString(output.EmptyOut())
//                operatorStr = ToString(operators.EmptyOut())
            } else if token.isLBracket {
                operators.push(token)
//                operatorStr = ToString(operators.EmptyOut())
            } else if token.isRBracket {
                rBracketOp(&output, &operators)
//                outputStr = ToString(output.EmptyOut())
//                operatorStr = ToString(operators.EmptyOut())
            } else {}
        }

        for op in operators.getContent() {
            var bracketLeft: Bool {
                if op == "(" || op == ")" {
                    return true
                } else {
                    return false
                }
            }
            assert(!bracketLeft, "RPN: Bracket mismatch.")
            output.enqueue(operators.pop()!)
        }

        outputStr = toString(output.getContent())

        return outputStr
    }

    private func loadExpr() -> String {
        //
        var expression: String = ""

        repeat {
            print("Expression:")
            let input = readLine()
            if let _input = input {
                expression = _input
                break
            } else {
                continue
            }
        } while(true)

        return expression
    }

    private func operatorOp(_ output: inout Queue<Substring>, _ operators: inout Stack<Substring>, _ op: Substring) {

        if !operators.isEmpty {
            var topOp = operators.top!

            while( ((topOp.prec > op.prec) || (topOp.prec == op.prec && op.assc == "L"))
                    && (topOp != "(") ) {
                if op == "(" {
                    break
                }
                output.enqueue(topOp)
                _ = operators.pop()

                if !operators.isEmpty {
                    topOp = operators.top!
                } else {
                    break
                }
            }
        }
        operators.push(op)
    }

    private func rBracketOp(_ output: inout Queue<Substring>, _ operators: inout Stack<Substring>) {
        //
        var foundMatchingBracket = false

        for topOp in operators.getContent() {
            if topOp.isLBracket {
                foundMatchingBracket = true
                break;
            } else {
                output.enqueue(operators.pop()!)
            }
        }
        _ = operators.pop()

        assert(foundMatchingBracket, "RBracketOp: Bracket mismatch.")
    }

    private func toString(_ array: [Substring]) -> String {
        var str = ""
        for _substr in array {
            str.append(contentsOf: _substr)
            str.append(" ")
        }
        return str
    }
}

struct Queue<T> {

    fileprivate var array = [T]()

    public var count: Int {
        return array.count
    }
    public var front: T? {
        return array.first
    }
    public var isEmpty: Bool {
        return array.isEmpty
    }

    public mutating func enqueue(_ element: T) {
        array.append(element)
    }

    public mutating func dequeue() -> T? {
        if isEmpty {
            return nil
        } else {
            return array.removeFirst()
        }
    }

    public func getContent() -> Array<T> {
        return array
    }
}

struct Stack<T> {

    fileprivate var array = [T]()

    public var count: Int {
        return array.count
    }
    public var isEmpty: Bool {
        return array.isEmpty
    }
    public var top: T? {
        return array.last
    }

    public mutating func push(_ element: T) {
        array.append(element)
    }

    public mutating func pop() -> T? {
        if isEmpty {
            return nil
        } else {
            return array.removeLast()
        }
    }

    public func getContent() -> Array<T> {
        return array.reversed()
    }
}

func PrintPostfix() {

    let postf = Postfix(expr: "33 + 4 * 2 / ( 1 - 5 ) ^ 2 ^ 3")

    print("\(postf.expr)")
}
